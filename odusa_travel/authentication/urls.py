
from .views import *
from django.urls import include, path, re_path



urlpatterns = [
    path('register/', RegisterAPI.as_view(), name='register'),
    path('email-verify', VerifyEmail.as_view(), name='email-verify'),
    path('login/', LoginAPI.as_view(), name='login'),
    #path('api/auth/user', UserAPI.as_view()),
    
]


