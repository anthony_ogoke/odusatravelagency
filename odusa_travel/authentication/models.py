from __future__ import unicode_literals

from django.db import models

from django.urls import reverse
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.utils.text import slugify
# Create your models here.
from django.contrib.auth.models import (AbstractBaseUser, BaseUserManager, PermissionsMixin)
from rest_framework_simplejwt.tokens import RefreshToken



class UserManager(BaseUserManager):
    """ User Manager that knows how to create users via email instead of username """
    def create_user(self, email, password=None):
        email = self.normalize_email(email)
        if email is None:
            raise TypeError('User should have an Email')
        user = self.model(email=email)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email=None, password=None):
        if password is None:
            raise TypeError('Password should not be none')
        user = self.create_user(email, password)
        user.is_superuser = True
        user.is_staff = True
        user.save()
        return user


class User(AbstractBaseUser, PermissionsMixin):
	objects 					=	UserManager()
	REQUIRED_FIELDS 			=	[]
	USERNAME_FIELD 				=	"email"
	username 					=	None
	email 						=	models.EmailField("email address", blank=False, null=False, unique=True)
	given_name 					=	models.CharField('first name', max_length=150, blank=True)
	family_name 				=	models.CharField('last name', max_length=150, blank=True)
	TYPEOFPERSON = (
		('Adult','Adult'),
		('Teen','Teen'),
		('Kid','Kid'),
	)
	age_type_person				=	models.CharField(blank=True, null=True, max_length=200, choices=TYPEOFPERSON)
	TITLE = (
		('Mr','Mr'),
		('Mrs','Mrs'),
		('Dr.','Dr.'),
	)
	title						=	models.CharField(blank=True, null=True, max_length=200, choices=TITLE)
	phone_number    			=	models.CharField(max_length=200, blank=True, null=True)
	born_on 					=	models.CharField(max_length=200, blank=True, null=True)
	is_verified                 =   models.BooleanField(default=False)
	is_active                   =   models.BooleanField(default=True)
	created_at                  =   models.DateTimeField(auto_now_add=True)
	updated                     =   models.DateTimeField(auto_now=True)
	unique_identifier			=	models.CharField(max_length=1000, blank=True, null=True)
	type_identifier				=	models.CharField(max_length=200, blank=True, null=True)
	issuing_country_code 		=	models.CharField(max_length=200, blank=True, null=True)
	expires_on 					=	models.CharField(max_length=1000, blank=True, null=True)


	def __str__(self):
		return self.email

	def tokens(self):
		tokens = RefreshToken.for_user(self)
		return {
	    	'refresh': str(refresh),
	    	'access': str(refresh.access_token)
	    }




