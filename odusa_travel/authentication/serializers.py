from rest_framework import serializers
from .models import *
from rest_framework.validators import UniqueValidator
from rest_framework.exceptions import AuthenticationFailed
from .models import User
from django.contrib import auth 

class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=68, min_length=8, write_only=True)

    class Meta:
        model = User
        #exclude = ('id', )
        fields = ('given_name', 'family_name', 'email', 'pasword')



class RegisterSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=68, min_length=8, write_only=True)

    class Meta:
        model = User
        fields = (
            'email',
            'password',
            'given_name',
            'family_name'
            )

        def validate(self, attrs):
            email = attrs.get('email', '')
            return super().validate(attrs)

        def create(self, validated_data):
            return User.objects.create_user(**validated_data)






class LoginSerializer(serializers.Serializer):

    email = serializers.EmailField(max_length=255, min_length=3)
    password = serializers.CharField(max_length=68, min_length=8, write_only=True)
    tokens = serializers.CharField(max_length=68, min_length=8, read_only=True)

    class Meta:
        model = User
        fields = ['email', 'password', 'tokens']

    def validate(self, attrs):
        email = attrs.get('email', '')
        password = attrs.get('password', '')

        user = auth.authenticate(email=email, password=password)

        if not user:
            raise AuthenticationFailed('Invalid credentials, try again')

        if not user.is_active:
            raise AuthenticationFailed('Account disabled, contact admin')

        if not user.is_verified:
            raise AuthenticationFailed('Email is not verified')

        
        
        return {
            'email': user.email,
            'tokens': user.tokens
        }



class EmailVerificationSerializer(serializers.Serializer):

    tokens = serializers.CharField(max_length=1000)

    class Meta:
        model = User
        fields = ['tokens']