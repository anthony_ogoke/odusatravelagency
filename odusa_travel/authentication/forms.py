from django import forms
from django.contrib.auth.forms import UserChangeForm, UserCreationForm 
from .models import User 
 
class CustomUserCreationForm(UserCreationForm):    
    class Meta:        
        model = User        
        fields = (
        	'email', 
        	'given_name', 
        	'family_name', 
        	'age_type_person', 
        	'title', 
        	'phone_number', 
        	'born_on',
        	
        	)  


class CustomUserChangeForm(UserChangeForm):    
    class Meta:        
        model = User        
        fields = UserChangeForm.Meta.fields