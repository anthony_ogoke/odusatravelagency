from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.utils.text import slugify
# Create your models here.

from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import UserManager as BaseUserManager



class UserManager(BaseUserManager):
    """ User Manager that knows how to create users via email instead of username """
    def _create_user(self, email, password, **extra_fields):
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email=None, password=None, **extra_fields):
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        return self._create_user(email, password, **extra_fields)

    def create_user(self, email=None, password=None, **extra_fields):
        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)
        return self._create_user(email, password, **extra_fields)



class User(AbstractUser):
	objects 					=	UserManager()
	REQUIRED_FIELDS 			=	[]
	USERNAME_FIELD 				=	"email"
	username 					=	None
	email 						=	models.EmailField("email address", blank=False, null=False, unique=True)
	given_name 					=	models.CharField('first name', max_length=150, blank=True)
	family_name 				=	models.CharField('last name', max_length=150, blank=True)
	TYPEOFPERSON = (
		('Adult','Adult'),
		('Teen','Teen'),
		('Kid','Kid'),
	)
	age_type_person				=	models.CharField(blank=True, null=True, max_length=200, choices=TYPEOFPERSON)
	TITLE = (
		('Mr','Mr'),
		('Mrs','Mrs'),
		('Dr.','Dr.'),
	)
	title						=	models.CharField(blank=True, null=True, max_length=200, choices=TITLE)
	phone_number    			=	models.CharField(max_length=200, blank=True, null=True)
	born_on 					=	models.CharField(max_length=200, blank=True, null=True)
	unique_identifier			=	models.CharField(max_length=1000, blank=True, null=True)
	type_identifier				=	models.CharField(max_length=200, blank=True, null=True)
	issuing_country_code 		=	models.CharField(max_length=200, blank=True, null=True)
	expires_on 					=	models.CharField(max_length=1000, blank=True, null=True)


	def __str__(self):
		return self.given_name + '' + self.family_name




