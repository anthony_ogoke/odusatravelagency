from rest_framework import routers
from .apis import UserViewSet
# from django.urls import include, path, re_path
# from . import views


# app_name = "authentication"


# urlpatterns = [
#     path("users", views.UserCreate.as_view(), name='account-create'),
#     path('auth', include('rest_auth.urls')),
#     path('auth/register/', include('rest_auth.registration.urls'))
# ]




router = routers.DefaultRouter()
router.register("api/users", UserViewSet, 'users')

urlpatterns = router.urls