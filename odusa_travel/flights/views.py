from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, Http404
from django.core.mail import send_mail, EmailMessage

from django.core.exceptions import ObjectDoesNotExist
from django.contrib import messages
from django.conf import settings
from rest_framework.viewsets import ModelViewSet
from rest_framework.parsers import JSONParser
from rest_framework.decorators import api_view
from rest_framework.response import Response


from .serializers import  *
from .models import *
from rest_framework import status


from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.views.decorators.csrf import csrf_exempt


import json
import requests

import uuid
import pprint



@api_view(['GET'])
def offer_viewset(request):
    if request.method == 'GET':
    	# information needed by Duffel's offer request endpoint
    	slices = [{
    		"origin": "NYC", 
    		"destination": "ATL", 
    		"departure_date": "2022-01-30"}]

    	#passengers information needed by duffel's offer request endpoint
    	passengers = [{"family_name": "Earhart", "given_name": "Amelia", "loyalty_programme_accounts": [{"account_number": "12901014", "airline_iata_code": "BA"}], "type": "adult"}, {"age": 14}]
    	
    	#header which will carry the access token
    	headers = {
    		'Authorization': 'Bearer duffel_test_Exh2-O_whLuj0Us9A5l8EraFZXHsjHdh4UWozKZTFiQ',
    		'Content-Type': 'application/json',
    		'Duffel-Version': 'beta',
    		'Accept': 'application/json',
    	}

    	#data needed by duffel's request offer endpoint
    	data = {"data": {"slices": slices, "passengers": passengers, "cabin_class": "business"}}

    	#Duffel's url endpoint
    	url = "https://api.duffel.com/air/offer_requests"

    	#response to be gotten from duffels offer request querry
    	response = requests.request("POST", url, headers=headers, json=data)
    	res = response.json()
    	data = res

    	#extracting list of offers
    	new_offers = res['data']['offers']
    	list_offer = [new_offers[0]]

    	for i in new_offers:

    		#getting the list of slices
    		slices = i['slices']

    		#getting offer request id
    		offer_request_id = i['id']

    		#getting payments requirements
    		payment_requirements = i['payment_requirements']['requires_instant_payment']
    		price_guarantee_expires_at= i['payment_requirements']['price_guarantee_expires_at']
    		payment_required_by= i['payment_requirements']['payment_required_by']

    		base_amount = i['base_amount']

    		#iterating through the list of slices to get each slice
    		for a in slices:

    			#In each slice grab the segment, iterating through the segment
    			for b in a['segments']:

    				# each segments
    				segment = b

    				#extracting destination in each segment
    				destination = segment['destination']

    				#extracting destination in each segment
    				origin = segment['origin']

    				#extracting operating carrier name in each segment
    				operating_carrier_name = segment['operating_carrier']['name']

    				#extra info to be displayed to customers
    				aircraft_id = segment['aircraft']['id']
    				aircraft_name = segment['aircraft']['name']
    				time_of_arrival = segment['arriving_at']
    				time_of_departure = segment['departing_at']

    				des_city_name = destination['city_name']
    				des_iata_code = destination['city']['iata_code']
    				des_iata_country_code = destination['city']['iata_country_code']
    				des_airport_name = destination['name']
    				des_time_zone = destination['time_zone']

    				origin_city_name = origin['city_name']
    				origin_iata_code = origin['city']['iata_code']
    				origin_iata_country_code = origin['city']['iata_country_code']
    				origin_airport_name = origin['name']
    				origin_time_zone = origin['time_zone']

    				tax_amount = i['tax_amount']
    				tax_currency = i['tax_currency']
    				total_amount = i['total_amount']
    				updated_at = i['updated_at']

    				#creating flight flight offer object in database
    				flight = FlightOffers(
    					offer_request_id=offer_request_id,
    					operating_carrier_name=operating_carrier_name,
    					payment_requirements=payment_requirements,
    					price_guarantee_expires_at=price_guarantee_expires_at,
    					payment_required_by=payment_required_by,
    					aircraft_id=aircraft_id,
    					aircraft_name=aircraft_name,
    					time_of_arrival=time_of_arrival,
    					time_of_departure=time_of_departure,
    					des_city_name=des_city_name,
    					des_iata_code=des_iata_code,
    					des_iata_country_code=des_iata_country_code,
    					des_airport_name=des_airport_name,
    					des_time_zone=des_time_zone,
    					origin_city_name=origin_city_name,
    					origin_iata_code=origin_iata_code,
    					origin_iata_country_code=origin_iata_country_code,
    					origin_airport_name=origin_airport_name,
    					origin_time_zone=origin_time_zone,
    					base_amount=base_amount,
    					tax_amount=tax_amount,
    					tax_currency=tax_currency,
    					total_amount=total_amount,
    					updated_at=updated_at

    				)

    	# 			flight.save()
    	# offers = FlightOffers.objects.all()
    	# serializer = FlightOfferSerializer(offers, many=True)
    	# return Response(serializer.data)
    	return Response(list_offer)



"https://api.duffel.com/air/offers?offer_request_id=$OFFER_REQUEST_ID&sort=total_amount"
"https://api.duffel.com/air/offers/$OFFER_ID" 


@api_view(['GET'])
def get_offer(request):
    if request.method == 'GET':
    	headers = {
    		'Authorization': 'Bearer duffel_test_Exh2-O_whLuj0Us9A5l8EraFZXHsjHdh4UWozKZTFiQ',
    		'Content-Type': 'application/json',
    		'Duffel-Version': 'beta',
    		'Accept': 'application/json',
    	}
    	id_offer = "off_00009htYpSCXrwaB9DnUm0"
    	url = f"https://api.duffel.com/air/offers/{id_offer}" 
    	response = requests.request("GET", url, headers=headers)
    	res = response.json()

    	#extracting list of offers
    	offer = res['data']['offers']
    	list_offer = [offer[0]]

    	for i in offer:

    		#getting the list of slices
    		slices = i['slices']

    		#getting offer request id
    		offer_request_id = i['id']

    		#getting payments requirements
    		pay = i['payment_requirements']
    		payment_requirements = pay['requires_instant_payment']
    		price_guarantee_expires_at= pay['price_guarantee_expires_at']
    		payment_required_by= pay['payment_required_by']

    		base_amount = i['base_amount']

    		#iterating through the list of slices to get each slice
    		for a in slices:

    			#In each slice grab the segment, iterating through the segment
    			for b in a['segments']:

    				# each segments
    				segment = b

    				#extracting destination in each segment
    				destination = segment['destination']

    				#extracting destination in each segment
    				origin = segment['origin']

    				#extracting operating carrier name in each segment
    				operating_carrier_name = segment['operating_carrier']['name']

    				#extra info to be displayed to customers
    				aircraft_id = segment['aircraft']['id']
    				aircraft_name = segment['aircraft']['name']
    				time_of_arrival = segment['arriving_at']
    				time_of_departure = segment['departing_at']

    				des_city_name = destination['city_name']
    				des_iata_code = destination['city']['iata_code']
    				des_iata_country_code = destination['city']['iata_country_code']
    				des_airport_name = destination['name']
    				des_time_zone = destination['time_zone']

    				origin_city_name = origin['city_name']
    				origin_iata_code = origin['city']['iata_code']
    				origin_iata_country_code = origin['city']['iata_country_code']
    				origin_airport_name = origin['name']
    				origin_time_zone = origin['time_zone']

    				tax_amount = i['tax_amount']
    				tax_currency = i['tax_currency']
    				total_amount = i['total_amount']
    				updated_at = i['updated_at']

    				#creating flight flight offer object in database
    				offer = Offer(
    					offer_request_id=offer_request_id,
    					operating_carrier_name=operating_carrier_name,
    					payment_requirements=payment_requirements,
    					price_guarantee_expires_at=price_guarantee_expires_at,
    					payment_required_by=payment_required_by,
    					aircraft_id=aircraft_id,
    					aircraft_name=aircraft_name,
    					time_of_arrival=time_of_arrival,
    					time_of_departure=time_of_departure,
    					des_city_name=des_city_name,
    					des_iata_code=des_iata_code,
    					des_iata_country_code=des_iata_country_code,
    					des_airport_name=des_airport_name,
    					des_time_zone=des_time_zone,
    					origin_city_name=origin_city_name,
    					origin_iata_code=origin_iata_code,
    					origin_iata_country_code=origin_iata_country_code,
    					origin_airport_name=origin_airport_name,
    					origin_time_zone=origin_time_zone,
    					base_amount=base_amount,
    					tax_amount=tax_amount,
    					tax_currency=tax_currency,
    					total_amount=total_amount,
    					updated_at=updated_at

    				)

    				offer.save()
  
    	offers = Offers.objects.get(offer_request_id=offer_request_id)
    	serializer = OfferSerializer(offers, many=True)
    	return Response(serializer.data)



#get the request_offer_id from url. Use it to fetch the offer in the database, you will need it to
#get the user who rquested the offer and is about to order.
@api_view(['GET'])
def order(request):
	#confirm if this will be a get or post request
    if request.method == 'GET':
    	headers = {
    		'Authorization': 'Bearer duffel_test_Exh2-O_whLuj0Us9A5l8EraFZXHsjHdh4UWozKZTFiQ',
    		'Content-Type': 'application/json',
    		'Duffel-Version': 'beta',
    		'Accept': 'application/json',
    	}
    	r = uuid.uuid4()
    	reference = str(r)

    	data = {
    		"passengers":[
    			{
    				"type": "adult",
    				"title": "mrs",
    				"phone_number": "+442080160509",
    				"infant_passenger_id": "pas_00009hj8USM8Ncg32aTGHL",
    				"identity_documents": [
    					{
    						"unique_identifier": "19KL56147",
    						"type": "passport",
    						"issuing_country_code": "GB",
    						"expires_on": "2025-04-25"
    					}
    				],
    				"id": "pas_00009hj8USM7Ncg31cBCLL",
    				"given_name": "Amelia",
    				"gender": "f",
    				"family_name": "Earhart",
    				"email": "amelia@duffel.com",
    				"born_on": "1987-07-24"
    			}
    		],
    		"metadata": {
    				"payment_intent_id": "pit_00009htYpSCXrwaB9DnUm2" #use your own generated reference here
    		},
    		"selected_offers": ["off_00009htyDGjIfajdNBZRlw"],
    		#To create an order and pay for it at the same time, 
    		#specify a payments key. To hold the order and pay for it later, 
    		#specify the type as hold, omit the payments and services keys, 
    		#and complete payment after creating the order through the 
    		#Create a payment endpoint."""

    		#The type of order. You can only use "hold" with offers where payment_requirements.requires_instant_payment is false.
    		#When booking an offer with type hold, do not specify payments or services keys.
    		#Possible values: "instant" or "hold"
    		"type": "instant",
    		"services": [
    			{
    				"quantity": 1,
    				"id": "ase_00009hj8USM7Ncg31cB123"
    			}
    		],
    		"payments": [
    			{
    				"type": "balance",
    				"currency": "GBP",
    				"amount": "30.20"
    			}
    		],
    	}


    	url = f"https://api.duffel.com/air/orders" 
    	response = requests.request("POST", url, headers=headers, json=data)
    	res = response.json()

    	
    	return Response(res)
    	#return JsonResponse(serializer.data, safe=False)
    	#return redirect(booking_offer)