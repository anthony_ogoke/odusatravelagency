from django.contrib import admin
from .models import *



class FlightOffersAdmin(admin.ModelAdmin):
    list_display = ['offer_request_id', 'origin_iata_code', 'des_iata_code', 'total_amount', 'operating_carrier_name']


admin.site.register(FlightOffers, FlightOffersAdmin)
