from django.urls import include, path
from rest_framework import routers
from . import views

urlpatterns = [
    path("", views.offer_viewset, name="offer_viewset"),
    path("get_offer", views.get_offer, name="get_offer"),
    #path("<int:flight_id>/book", views.booking_offer, name="booking_offer")
]



