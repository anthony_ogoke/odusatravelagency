from __future__ import unicode_literals

from django.db import models
from django.urls import reverse
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.utils.text import slugify
# Create your models here.

from authentication.models import User


# Create your models here.
class Airport(models.Model):
    code = models.CharField(max_length=3)
    city = models.CharField(max_length=64)

    def __str__(self):
        return f"{self.city} ({self.code})"




class Flight(models.Model):

	

	user 				=      	models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_flights')
	
	destination			= 		models.CharField(max_length=200, null=True, blank=True)
	origin				= 		models.CharField(max_length=200, null=True, blank=True)
	departure_date		= 		models.CharField(max_length=200, null=True, blank=True)
	arrival_date		= 		models.CharField(max_length=200, null=True, blank=True)
	reference			=		models.CharField(max_length=500, blank=True, null=True)

	class Meta:
		ordering = ['-id']
		verbose_name = 'Flight'
		verbose_name_plural = 'Flights'

	def __str__(self):
		return f"{self.id} : {self.origin} to {self.destination}"



class Passenger(models.Model):

	TYPEOFPERSON = (
		('Adult','Adult'),
		('Teen','Teen'),
		('Kid','Kid'),
	)
	first			=		models.CharField(max_length=64)
	last			=		models.CharField(max_length=64)
	type_person		=		models.CharField(blank=True, null=True, max_length=200, choices=TYPEOFPERSON)
	flights 		=		models.ManyToManyField(Flight, blank=True, related_name="passenger")

	def __str__(self):
		return f"{self.first} {self.last}"



class FlightOffers(models.Model):

	user 						=		models.ManyToManyField(User, blank=True, related_name="passengers")
	offer_request_id			=		models.CharField(blank=True, null=True, max_length=64)
	operating_carrier_name		=		models.CharField(blank=True, null=True, max_length=64)
	payment_requirements		=		models.CharField(blank=True, null=True, max_length=64)
	price_guarantee_expires_at	=		models.CharField(blank=True, null=True, max_length=64)
	payment_required_by			=		models.CharField(blank=True, null=True, max_length=64)

	aircraft_id					=		models.CharField(blank=True, null=True, max_length=64)
	aircraft_name				=		models.CharField(blank=True, null=True, max_length=64)
	time_of_arrival				=		models.CharField(blank=True, null=True, max_length=64)
	time_of_departure 			=		models.CharField(blank=True, null=True, max_length=64)

	des_city_name				=		models.CharField(blank=True, null=True, max_length=64)
	des_iata_code				=		models.CharField(blank=True, null=True, max_length=64)
	des_iata_country_code		=		models.CharField(blank=True, null=True, max_length=64)
	des_airport_name 			=		models.CharField(blank=True, null=True, max_length=64)
	des_time_zone				=		models.CharField(blank=True, null=True, max_length=64)

	origin_city_name			=		models.CharField(blank=True, null=True, max_length=64)
	origin_iata_code			=		models.CharField(blank=True, null=True, max_length=64)
	origin_iata_country_code	=		models.CharField(blank=True, null=True, max_length=64)
	origin_airport_name 		=		models.CharField(blank=True, null=True, max_length=64)
	origin_time_zone			=		models.CharField(blank=True, null=True, max_length=64)

	base_amount					=		models.CharField(blank=True, null=True, max_length=64)
	tax_amount 					=		models.CharField(blank=True, null=True, max_length=64)
	tax_currency 				=		models.CharField(blank=True, null=True, max_length=64)
	total_amount 				=		models.CharField(blank=True, null=True, max_length=64)
	updated_at 					=		models.CharField(blank=True, null=True, max_length=64)


	def __str__(self):
		return f"{self.aircraft_id} : {self.origin_iata_code} to {self.des_iata_code}"



class Offer(models.Model):

	user 						=		models.ManyToManyField(User, blank=True, related_name="user_passengers")
	offer_request_id			=		models.CharField(blank=True, null=True, max_length=64)
	operating_carrier_name		=		models.CharField(blank=True, null=True, max_length=64)
	payment_requirements		=		models.CharField(blank=True, null=True, max_length=64)
	price_guarantee_expires_at	=		models.CharField(blank=True, null=True, max_length=64)
	payment_required_by			=		models.CharField(blank=True, null=True, max_length=64)

	aircraft_id					=		models.CharField(blank=True, null=True, max_length=64)
	aircraft_name				=		models.CharField(blank=True, null=True, max_length=64)
	time_of_arrival				=		models.CharField(blank=True, null=True, max_length=64)
	time_of_departure 			=		models.CharField(blank=True, null=True, max_length=64)

	des_city_name				=		models.CharField(blank=True, null=True, max_length=64)
	des_iata_code				=		models.CharField(blank=True, null=True, max_length=64)
	des_iata_country_code		=		models.CharField(blank=True, null=True, max_length=64)
	des_airport_name 			=		models.CharField(blank=True, null=True, max_length=64)
	des_time_zone				=		models.CharField(blank=True, null=True, max_length=64)

	origin_city_name			=		models.CharField(blank=True, null=True, max_length=64)
	origin_iata_code			=		models.CharField(blank=True, null=True, max_length=64)
	origin_iata_country_code	=		models.CharField(blank=True, null=True, max_length=64)
	origin_airport_name 		=		models.CharField(blank=True, null=True, max_length=64)
	origin_time_zone			=		models.CharField(blank=True, null=True, max_length=64)

	base_amount					=		models.CharField(blank=True, null=True, max_length=64)
	tax_amount 					=		models.CharField(blank=True, null=True, max_length=64)
	tax_currency 				=		models.CharField(blank=True, null=True, max_length=64)
	total_amount 				=		models.CharField(blank=True, null=True, max_length=64)
	updated_at 					=		models.CharField(blank=True, null=True, max_length=64)


	def __str__(self):
		return f"{self.aircraft_id} : {self.origin_iata_code} to {self.des_iata_code}"

